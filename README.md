**latest code is in https://gitlab.ethz.ch/srl/3d-soft-trunk and is closed-source**
# SoPrA Model repository
## get this repository

In the desired directory, run...
```bash
## option 1: clone with https- enter username & password each time you access remote
git clone --recursive https://gitlab.ethz.ch/srl/sopramodel.git 
```
or
```bash
## option 2: clone with SSH- need to set up SSH key in GitLab, no username / password required
git clone --recursive git@gitlab.ethz.ch:srl/sopramodel.git
```
(`--recursive` option will automatically clone the submodules as well)

## set up WSL (for Windows)
1. Get [Ubuntu 20.04 from the Microsoft Store](https://www.microsoft.com/store/productId/9n6svws3rx71). If you don't need GUI, no further steps needed.

### suggested tutorials
* https://docs.microsoft.com/en-us/learn/modules/get-started-with-windows-subsystem-for-linux/
* https://ubuntu.com/tutorials/command-line-for-beginners

### to enable GUI in WSL
1. Make sure you're running WSL **2** (check by running `wsl -l -v` in Windows command line), if on WSL **1**, [refer to this](https://docs.microsoft.com/en-us/windows/wsl/install-win10) and update to WSL 2.
1. Install [VcXsrv](https://sourceforge.net/projects/vcxsrv/). This will be used for X11 forwarding in order to use GUI.
1. Launch VcXsrv with settings: *Multiple windows* -> *Start no client* -> check all except *Native opengl*
1. add to end of ~/.bashrc
    ```bash
    export DISPLAY=$(awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
    export LIBGL_ALWAYS_INDIRECT=0
    ```
    and open new terminal or run `source ~/.bashrc`.
1. GUI should work now! Try it out with `xeyes`, `xcalc`, `xclock` etc (need to install with `sudo apt install x11-apps`) (reboot may be needed).
1. For some PCs, by checking *Native opengl* in VcXsrv and adding `export LIBGL_ALWAYS_INDIRECT=1` to ~/.bashrc, OpenGL can be used.

## install necessary packages
(also check Dockerfile for hints on how to setup Ubuntu)

for Ubuntu

```bash
sudo apt update
sudo apt install cmake libmodbus-dev libeigen3-dev libserialport-dev libopencv-dev
sudo apt install python3-dev python3-numpy # install developer package and numpy for system's default python3 version.
```

Cmake version should be above 3.12 (check with `cmake --version`). Ubuntu 18.04 default cmake is older than that, so upgrade may be necessary, in which case run
```bash
# refer to: https://graspingtech.com/upgrade-cmake/
sudo apt remove --purge cmake
sudo apt update
sudo apt install build-essential libssl-dev
wget https://github.com/Kitware/CMake/releases/download/v3.16.5/cmake-3.16.5.tar.gz
tar -zxvf cmake-3.16.5.tar.gz
cd cmake-3.16.5
./bootstrap
make 
sudo make install
```

for macOS (todo: unverified)

```bash
brew install libmodbus eigen libserialport numpy opencv
```

also install these packages:
```bash
pip3 install xacro # used to convert robot model files from XACRO to URDF
```

## install Drake

refer to [Drake documentation- binary installation](https://drake.mit.edu/from_binary.html) and [Drake sample CMake project](https://github.com/RobotLocomotion/drake-external-examples/tree/master/drake_cmake_installed).

For Ubuntu 18.04, basic steps are:
```bash
curl -O https://drake-packages.csail.mit.edu/drake/nightly/drake-latest-bionic.tar.gz
## decompress and place drake files into /opt/drake
sudo tar -xvzf drake-latest-bionic.tar.gz -C /opt
## install prerequisites
sudo /opt/drake/share/drake/setup/install_prereqs
```

For macOS, .... TODO

## Compile

```bash
cd /path/to/3d-soft-trunk
cmake -DCMAKE_PREFIX_PATH=/opt/drake .
make
```

executables are output to bin, libraries are output to lib/.

## Python interface
In its current implementation, you must set the `$PYTHONPATH` environment variable to point to the directory containing the library binaries in order to run. (probably `sopramodel/lib`)

```bash
## run this everytime you open a new terminal to run a python script using this library
export PYTHONPATH=$PYTHONPATH:/path/to/lib
## Alternatively, append the line to ~/.bashrc if you don't want to run it every time.
python3
>> import softtrunk_pybind_module
>> aar = softtrunk_pybind_module.AugmentedRigidArm()
>> aar.update([0]*6, [0]*6)
```

see more examples in `examples_python/`.

## Generating Documentation

Uses Doxygen to generate documentation from inline comments in code. Install [Doxygen](http://www.doxygen.nl), and
run `doxygen Doxyfile` in this directory to generate HTML (can be seen with browser at html/index.html) & LATEX output.
